module.exports = function(eleventyConfig) {
    eleventyConfig.addPassthroughCopy("css/bundle.css");
    eleventyConfig.addPassthroughCopy("css/chota.css");
    eleventyConfig.addPassthroughCopy("img");
    eleventyConfig.addPassthroughCopy("js");

    return {
        dir: {
          output: "public"
        }
      }
};